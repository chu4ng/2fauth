# 2F-Auth microservice implementation

To run a service working postgres instance is required. Credentials for db and another variables are set in config.yaml
## Endpoints
### /v1/send
Receive:
```json
{
  "number": "+334632325"
}
```
Response with
```json
{
  "requestId": "2d692419-e758-4901-8fe2-89862f660c06",
  "code": "047204"
}
```
### /v1/verify
Receive:
```json
{
  "requestId": "2d692419-e758-4901-8fe2-89862f660c06",
  "code": "047204"
}
```
Response with
```json
{
  "verifiedAt": "1682408590"
}
```

If code doesn't match generated one, it gives you an error. If code is incorrect N times, this request will be banned.
It also supports time-to-live, so after the specified period of time request will set as expired.
