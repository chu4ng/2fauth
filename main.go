package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"gopkg.in/yaml.v3"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"twofactor/api"
	. "twofactor/auth"
	"twofactor/repository/inmemory"
	"twofactor/store/pg"
)

func main() {
	conf, err := loadConfiguration("config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	ttl := 20 * time.Second

	repo, err := createRepo(&conf.StoreCredentials)
	if err != nil {
		log.Fatal(err)
	}
	service := NewTFService(repo, TFServiceOptions{Ttl: ttl, Conf: conf})
	handler := api.NewHandler(service)

	router := chi.NewRouter()
	router.Route("/v1", func(r chi.Router) {

		r.Post("/send", handler.Send)
		r.Post("/verify", handler.Verify)
	})

	errs := make(chan error, 2)
	go func() {
		log.Printf("Listening on port :%d", conf.Port)
		errs <- http.ListenAndServe(fmt.Sprintf("%s:%d", conf.Address, conf.Port), router)

	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	log.Printf("Terminated %s", <-errs)
}

func loadConfiguration(configFileName string) (TFAuthConfiguration, error) {
	confFile, err := os.ReadFile(configFileName)
	if err != nil {
		return TFAuthConfiguration{}, err
	}

	conf := TFAuthConfiguration{}
	err = yaml.Unmarshal(confFile, &conf)
	if err != nil {
		return TFAuthConfiguration{}, err
	}

	return conf, nil
}

func createRepo(dbCred *StoreCredentials) (TFRepository, error) {
	var (
		store TFStorage
		repo  TFRepository
		err   error
	)
	store, err = pg.NewPgRepository(dbCred)
	if err != nil {
		return nil, err
	}
	repo, err = inmemory.NewInMemoryRepository(store)
	return repo, err
}
