package auth

import (
	"errors"
	"time"
)

type TFService interface {
	Verify(rid, code string) (time.Time, error)
	Check(redirect *Request, code string) error
	Save(redirect *Request) error
}

var (
	ErrRequestNotFound     = errors.New("ID not found")
	ErrRequestInvalidPhone = errors.New("invalid phone number")
	ErrRequestWrongCode    = errors.New("invalid code")
	ErrRequestBanned       = errors.New("request's banned")
	ErrRequestExpire       = errors.New("request expired")
)

type tfService struct {
	tfRepo  TFRepository
	options TFServiceOptions
}

type TFServiceOptions struct {
	Ttl  time.Duration
	Conf TFAuthConfiguration
}

type TFAuthConfiguration struct {
	AuthCodeLength  int `yaml:"authCodeLength"`
	MaxAuthAttempts int `yaml:"maxAuthAttempts"`

	Address          string `yaml:"address"`
	Port             int    `yaml:"port"`
	StoreCredentials `yaml:"storeCredentials"`
}
