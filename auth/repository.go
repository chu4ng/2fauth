package auth

type TFRepository interface {
	Find(code string) (*Request, error)
	TFContainer
}

type TFStorage interface {
	// Load requests from disk, db or other resources
	Load() ([]Request, error)
	TFContainer
}

type TFContainer interface {
	Save(request *Request) error
	IncrementAttempt(request *Request) error
}

type StoreCredentials struct {
	UserName string `yaml:"userName"`
	Password string `yaml:"password"`
	Dbname   string `yaml:"dbname"`
	Timeout  int    `yaml:"timeoutInSeconds"`
}
