package auth

import (
	"github.com/google/uuid"
	"math/rand"
	"strconv"
	"time"
)

func NewTFService(tfRepo TFRepository, options TFServiceOptions) TFService {
	return &tfService{
		tfRepo,
		options,
	}
}

// Verify check if:
//   - Request.RequestId is present
//   - number of attempts to verify is less than N
//   - time-to-live isn't expired
//   - Auth code is correct
//
// return current time in unix-format if all checks pass
func (t *tfService) Verify(rid, code string) (time.Time, error) {
	req, err := t.tfRepo.Find(rid)
	if err != nil {
		return time.Time{}, err
	}

	return time.Now(), t.Check(req, code)

}

func (t *tfService) Check(req *Request, code string) error {
	var err error
	// Depending on service traffic I'd set flags in struct instead of checking each request
	if req.Attempt >= t.options.Conf.MaxAuthAttempts {
		return ErrRequestBanned
	}

	if req.isExpire(t.options.Ttl) {
		return ErrRequestExpire
	}
	if code != req.AuthCode {
		if err = t.tfRepo.IncrementAttempt(req); err != nil {
			return err
		}
		return ErrRequestWrongCode
	}
	return nil

}

// Save create Request for given number
func (t *tfService) Save(req *Request) error {
	if len(req.PhoneNumber) == 0 {
		return ErrRequestInvalidPhone
	}
	req.CreateRequest(t.options.Conf.AuthCodeLength)

	err := t.tfRepo.Save(req)
	return err
}

func (req *Request) CreateRequest(codeLength int) {
	req.RequestId = newRequestId()
	req.AuthCode = newAuthCode(codeLength)
	req.Attempt = 0
	req.CreatedAt = time.Now()
}

func newRequestId() string {
	return uuid.NewString()
}

var seededRand = rand.New(rand.NewSource(time.Now().Unix()))

// newAuthCode generate string of random numbers with variable length
func newAuthCode(codeLength int) string {
	newCode := ""
	for i := 0; i < codeLength; i++ {
		newCode += strconv.Itoa(seededRand.Intn(9))
	}
	return newCode
}

func (req *Request) isExpire(ttl time.Duration) bool {
	return time.Now().After(req.CreatedAt.Add(ttl))
}
