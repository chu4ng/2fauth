package auth

import "time"

type Request struct {
	Attempt   int
	CreatedAt time.Time
	// Example: "+791099949a94"
	PhoneNumber string `json:"number" binding:"required" validate:"required,email"`
	// Example: "f2441c3c-5970-43ba-a952-ed18a00d5fa6"
	RequestId string `pg:",pk"`
	// Example: "1234" with variable length
	AuthCode string
}
