package auth

import (
	"log"
	"testing"
	"time"
)

var (
	ttl           = 30 * time.Second
	maxAttemptNum = 3
	codeLength    = 4
)

func TestRequest_isExpire(t *testing.T) {
	createdAt := time.Now().Add(-10 * time.Second)

	r := &Request{CreatedAt: createdAt}
	if r.isExpire(30 * time.Second) {
		t.Fatal("Failed valid req")
	}
	r = &Request{CreatedAt: createdAt}
	if !r.isExpire(20) {
		t.Fatal("False-positive")
	}
}

func setupService() TFService {
	store := &InMemoryRepository{data: []Request{}}
	conf := TFAuthConfiguration{AuthCodeLength: codeLength, MaxAuthAttempts: maxAttemptNum}
	s := NewTFService(store, TFServiceOptions{Ttl: ttl, Conf: conf})
	return s
}

var service = setupService()

func TestTfService_Verify_NotFound(t *testing.T) {
	req := Request{PhoneNumber: "+79109994994"}
	req.CreateRequest(codeLength)

	_, err := service.Verify("Nothingness", req.AuthCode)
	if err != ErrRequestNotFound {
		t.Fail()
	}

}
func TestTfService_Check_Correct(t *testing.T) {
	req := Request{PhoneNumber: "+79109994994"}
	err := service.Save(&req)
	if err != nil {
		t.Fatal(err)
	}

	// correct request
	unixtime, err := service.Verify(req.RequestId, req.AuthCode)
	if err != nil {
		t.Fatal(err)
	}
	now := time.Now()
	if unixtime.Second() != now.Second() {
		t.Fatal("time should be equal")
	}

}

func TestTfService_Check_Banned(t *testing.T) {
	var err error
	req := Request{PhoneNumber: "+79109994994"}
	req.CreateRequest(codeLength)

	for i := 0; i < 3+1; i++ {
		err = service.Check(&req, "notRealCode")
	}
	if err != ErrRequestBanned {
		log.Print(err)
		t.Fatalf("Shall be banned after %d attempt\n", 3)
	}

}

func TestTfService_Check_Expire(t *testing.T) {
	req := Request{PhoneNumber: "+79109994994"}
	req.CreateRequest(codeLength)

	req.CreatedAt = req.CreatedAt.Add(-120 * time.Second)
	err := service.Check(&req, req.AuthCode)
	if err != ErrRequestExpire {
		t.Fail()
	}

}
func TestTfService_Save_InvalidPhone(t *testing.T) {
	req := Request{PhoneNumber: ""}
	err := service.Save(&req)
	if err != ErrRequestInvalidPhone {
		t.Fail()
	}
}

type InMemoryRepository struct {
	data []Request
}

func (i *InMemoryRepository) Find(rid string) (*Request, error) {
	for j, request := range i.data {
		if rid == request.RequestId {
			return &(i.data[j]), nil
		}
	}
	return nil, ErrRequestNotFound
}

func (i *InMemoryRepository) Save(req *Request) error {
	i.data = append(i.data, *req)
	return nil
}
func (i *InMemoryRepository) IncrementAttempt(req *Request) error {
	(*req).Attempt += 1
	return nil
}
