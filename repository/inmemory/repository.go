package inmemory

import (
	. "twofactor/auth"
)

type InMemoryRepository struct {
	data      []Request
	diskStore TFStorage
}

func NewInMemoryRepository(store TFStorage) (TFRepository, error) {
	reds, err := store.Load()
	if err != nil {
		return nil, err
	}
	return &InMemoryRepository{diskStore: store, data: reds}, nil
}

func (i *InMemoryRepository) Find(rid string) (*Request, error) {
	for j, request := range i.data {
		if rid == request.RequestId {
			return &(i.data[j]), nil
		}
	}
	return nil, ErrRequestNotFound
}

func (i *InMemoryRepository) Save(req *Request) error {
	i.data = append(i.data, *req)
	return i.diskStore.Save(req)
}
func (i *InMemoryRepository) IncrementAttempt(req *Request) error {
	return i.diskStore.IncrementAttempt(req)
}
