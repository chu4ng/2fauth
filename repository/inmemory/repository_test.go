package inmemory

import (
	"testing"
	"time"
	"twofactor/auth"
)

func TestInMemoryRepository_Find(t *testing.T) {
	controlRequest := auth.Request{
		RequestId: "6dae2efe-0869-4936-a2cb-1585efd7fed2", AuthCode: "321", PhoneNumber: "+84245688923", CreatedAt: time.Now().Add(-10 * time.Second), Attempt: 1,
	}
	tests := []auth.Request{{
		RequestId: "c3cdded4-43f0-44c9-9bb4-72b217b1e999", AuthCode: "123", PhoneNumber: "+79109994994", CreatedAt: time.Now().Add(-10 * time.Second), Attempt: 1,
	},
		controlRequest,
		{
			RequestId: "2c41a067-0e11-47d3-891e-a5cb8857e43b", AuthCode: "333", PhoneNumber: "+89175968267", CreatedAt: time.Now().Add(-10 * time.Second), Attempt: 1,
		},
	}

	inMem := InMemoryRepository{data: tests}
	reqFound, err := inMem.Find(controlRequest.RequestId)
	if err != nil {
		t.Fatal(err)
	}
	if *reqFound != controlRequest {
		t.Fatal("Found req doesn't match")
	}
	_, err = inMem.Find("NotRealUUID")
	if err != auth.ErrRequestNotFound {
		t.Fatal("Shall make an issue")
	}
}

func TestInMemoryRepository_Save(t *testing.T) {
	req := auth.Request{
		RequestId: "6dae2efe-0869-4936-a2cb-1585efd7fed2", AuthCode: "321", PhoneNumber: "+84245688923", CreatedAt: time.Now().Add(-10 * time.Second), Attempt: 1,
	}
	inMem := InMemoryRepository{data: []auth.Request{}, diskStore: StorageMock{}}
	err := inMem.Save(&req)
	if err != nil {
		t.Fatal(err)
	}

	fReq, _ := inMem.Find(req.RequestId)
	if fReq == nil {
		t.Fatal("Found nothing")
	}
}

type StorageMock struct {
	data []auth.Request
}

func (s StorageMock) Load() ([]auth.Request, error) {
	return []auth.Request{}, nil
}

func (s StorageMock) Save(_ *auth.Request) error {
	return nil
}

func (s StorageMock) IncrementAttempt(_ *auth.Request) error {
	return nil
}
