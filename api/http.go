package api

import (
	"encoding/json"
	"log"
	"net/http"
	"twofactor/auth"
)

type TFHandler interface {
	Send(http.ResponseWriter, *http.Request)
	Verify(http.ResponseWriter, *http.Request)
}

type handler struct {
	tfService auth.TFService
}

func NewHandler(tfService auth.TFService) TFHandler {
	return &handler{tfService: tfService}
}

type sendEndpoint struct {
	Number string `json:"number"`
}

type responseEndpoint struct {
	RequestId string `json:"requestId"`
	Code      string `json:"code"`
}

// Send receive phone number
// and return requestId and authCode
func (h *handler) Send(w http.ResponseWriter, r *http.Request) {
	var jsonReq sendEndpoint
	err := json.NewDecoder(r.Body).Decode(&jsonReq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
		log.Print(err)
		return
	}
	number := jsonReq.Number

	authRequest := auth.Request{PhoneNumber: number}
	err = h.tfService.Save(&authRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
		log.Print(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	jsonResp, err := json.Marshal(responseEndpoint{RequestId: authRequest.RequestId, Code: authRequest.AuthCode})
	if err != nil {
		http.Error(w, err.Error(), 520)
		log.Print(err)
		return
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		http.Error(w, err.Error(), 520)
		log.Print(err)
	}

}

type verifyResponse struct {
	VerifiedAt int64 `json:"verifiedAt"`
}

// Verify receive requestId and authCode
// and return unixtime of verifying or error
func (h *handler) Verify(w http.ResponseWriter, r *http.Request) {
	// TODO send error as json
	var jsonReq responseEndpoint
	err := json.NewDecoder(r.Body).Decode(&jsonReq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
		log.Print(err)
		return
	}
	unixtime, err := h.tfService.Verify(jsonReq.RequestId, jsonReq.Code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
		log.Print(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	jsonResp, err := json.Marshal(verifyResponse{unixtime.Unix()})
	if err != nil {
		http.Error(w, err.Error(), 520)
		log.Print(err)
		return
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		http.Error(w, err.Error(), 520)
		log.Print(err)
	}
}
