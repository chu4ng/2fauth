package pg

import (
	"context"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"time"
	"twofactor/auth"
)

type pgRepository struct {
	client *pg.DB
}

func newPgClient(pgUser, pgName string, pgTimeout int) (*pg.DB, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(pgTimeout)*time.Second)
	defer cancel()
	db := pg.Connect(&pg.Options{
		User:     pgUser,
		Database: pgName,
	})
	if err := db.Ping(ctx); err != nil {
		return nil, err
	}
	err := createSchema(db)
	return db, err
}

func createSchema(db *pg.DB) error {
	err := db.Model(&auth.Request{}).CreateTable(&orm.CreateTableOptions{
		Temp:        false,
		IfNotExists: true,
	})
	return err
}

func NewPgRepository(cred *auth.StoreCredentials) (auth.TFStorage, error) {
	repo := &pgRepository{}
	client, err := newPgClient(cred.UserName, cred.Dbname, cred.Timeout)
	repo.client = client
	return repo, err
}

func (r *pgRepository) Load() ([]auth.Request, error) {
	var redirects []auth.Request
	err := r.client.Model(&redirects).Select()
	return redirects, err
}

func (r *pgRepository) Save(request *auth.Request) error {
	_, err := r.client.Model(request).Insert()
	return err
}

func (r *pgRepository) IncrementAttempt(req *auth.Request) error {
	err := r.client.Model(req).WherePK().Select()
	if err != nil {
		return err
	}

	(*req).Attempt += 1
	_, err = r.client.Model(req).WherePK().Update()
	return err
}
